package controllers;

import models.Requests;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;

public class KnowMyDriverController extends Controller{
	
	@CorsComposition.Cors
	public Result knowMyDriver() {
		
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		
		if(session != null) {
			JsonNode drivers = Requests.whoIsMyDriver(session);
			if(drivers == null)
			return badRequest("Something went wrong!");
			else return ok(drivers);
		}
		else return badRequest(APIconstants.SESSION_KEY_ERROR);
	}

}
