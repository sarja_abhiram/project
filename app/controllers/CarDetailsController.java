package controllers;

import java.util.List;

import models.Session;
import models.User;
import models.UserCar;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class CarDetailsController extends Controller{

	@CorsComposition.Cors
	public Result addCarDetails() throws Exception {

		JsonNode json = request().body().asJson();

		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);

		String dlNumber = null;
		String carName = null;
		int carFlag = 0;

		if(session != null) {
			try {
				
				if(json == null) {
					return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
				}
				
				if(json.findPath(APIconstants.DRIVER_LICENCE).textValue() != null && !json.findPath(APIconstants.DRIVER_LICENCE).textValue().isEmpty()){
					dlNumber = json.findPath(APIconstants.DRIVER_LICENCE).textValue();
				}
				else{
					throw new Exception(APIconstants.DRIVER_LICENCE_INVALID);
				}

				if(json.findPath(APIconstants.CAR_NAME).textValue() != null && !json.findValue(APIconstants.CAR_NAME).textValue().isEmpty()) {
					carName = json.findPath(APIconstants.CAR_NAME).textValue();
				}
				else {
					throw new Exception(APIconstants.CAR_NAME_ERROR);
				}

				int comfortLevel = json.findPath(APIconstants.COMFORT_LEVEL).intValue();
				if(comfortLevel <= 0 && comfortLevel > 5) {
					throw new Exception(APIconstants.COMFORT_LEVEL_INVALID);
				}

				User.updateUserDrivingLicence(dlNumber, session.userId);

				List<UserCar> usercarList = UserCar.getUserCarByUserId(session.userId);
				for(UserCar userCar : usercarList) {
					if(userCar.carName.equals(carName)) {
						carFlag = 1;
						break;
					}
				}

				if(carFlag == 0) {
					UserCar.CreateNewUserCar(session.userId, carName, comfortLevel);
				}
				return ok();

			}
			catch(Exception e) {
				return badRequest(e.getLocalizedMessage());
			}
		}
		else {
			return badRequest(APIconstants.SESSION_KEY_ERROR);
		}

	}
	
	@CorsComposition.Cors
	public Result getCarDetails() {
		
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		JsonNode carsJson = Json.newObject();
		
		if(session != null) {
			List<UserCar> userCarList = UserCar.getUserCarByUserId(session.userId);
			carsJson = Json.toJson(userCarList);
			return ok(carsJson);
		}
		else {
			return badRequest(APIconstants.SESSION_KEY_ERROR);
		}
	}
	
	
	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);
		return ok(result);
	}
}