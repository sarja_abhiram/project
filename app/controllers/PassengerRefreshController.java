package controllers;

import models.Requests;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;

public class PassengerRefreshController extends Controller{

	@CorsComposition.Cors
	public Result DriverToPassenger() {
	
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);

	try {
		
		if(session != null) {				
			JsonNode driver = Requests.notifyPassenger(session);
			if(driver.size() == 0) {
				return ok(APIconstants.NO_DRIVERS);
			}
			return ok(driver);
		}
		else return badRequest(APIconstants.SESSION_KEY_ERROR);
	
	}
	catch(Exception e) {
		e.printStackTrace();
		return badRequest("Something went wrong!");
		}
	}
}
