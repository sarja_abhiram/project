package controllers;

import models.RideRequest;
import models.Session;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.node.ObjectNode;


public class LoadPassengerDetailsController extends Controller{

	@CorsComposition.Cors
	public Result loadPassengerDetails() throws Exception{
		
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		ObjectNode result = Json.newObject();
		
		if(session != null) {
			try {
				result = RideRequest.loadRideRequestDetails(session);
				return ok(result);
			}
			catch(Exception e) {
				return badRequest(APIconstants.RIDE_REQUEST_ID_NOT_FOUND);
			}
		}
		else return badRequest(APIconstants.SESSION_KEY_ERROR);
	}
}
