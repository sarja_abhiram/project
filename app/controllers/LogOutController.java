package controllers;

import models.Session;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class LogOutController extends Controller{

	@CorsComposition.Cors
	public Result logOut() {
		
		String sessionKey = request().getHeader("sessionKey");
		Session session = Session.getSession(sessionKey);
		
		if(session != null)
		try{
			SqlUpdate down = Ebean.createSqlUpdate("DELETE FROM session WHERE session_id = :param1 ");
		    down.setParameter("param1", sessionKey);
		    down.execute();
			return ok();
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest();
		}
		else return badRequest();
	}
	
	@CorsComposition.Cors
	public Result logOutAgain() throws Exception{
		
		JsonNode json = request().body().asJson();
		
		String userName = null;
		String password = null;

		if(json == null) {
			return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
		}
		
		try{
			if(json.findPath(APIconstants.USER_NAME).textValue() != null && !json.findValue(APIconstants.USER_NAME).textValue().isEmpty()) {
				userName = json.findPath(APIconstants.USER_NAME).textValue();
			}
			else throw new Exception(APIconstants.USERNAME_ERROR);

			if(json.findPath(APIconstants.PASSWORD).textValue() != null && !json.findValue(APIconstants.PASSWORD).textValue().isEmpty()) {
				password = json.findPath(APIconstants.PASSWORD).textValue();
			}
			else throw new Exception(APIconstants.PASSWORD_ERROR);
		}
		catch(Exception e) {
			e.getLocalizedMessage();
			return badRequest(e.getLocalizedMessage());
		}
		
		if(Session.isSessionExists(userName)) {
			try {
				if(User.validateUsernameAndPasswordAgain(userName, password)) {
					SqlUpdate down = Ebean.createSqlUpdate("DELETE FROM session WHERE user_name = :param1 ");
				    down.setParameter("param1", userName);
				    down.execute();
				    return ok(APIconstants.SUCCESS);
				}
				else throw new Exception("Something went wrong");
			}
			catch(Exception e) {	
				return badRequest(e.getLocalizedMessage());
			}
		}
		else return badRequest(APIconstants.SESSION_NOT_EXISTS);

	}
	
	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);
		return ok(result);
	}
}
