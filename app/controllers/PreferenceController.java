package controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import models.RideRequest;
import models.Session;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class PreferenceController extends Controller {

	@CorsComposition.Cors
	public Result requestRide() throws Exception{

		JsonNode json = request().body().asJson();
		ObjectNode responseJson = Json.newObject();

		String sourceArea = null;
		String destArea = null;
		int seats = 0;

		try {
			
			if (json == null) {
				return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
			}

			String sessionKey = request().getHeader(APIconstants.SESSION_KEY);

			Session session = Session.getSession(sessionKey);
			if (session != null) {
				if(json.findValue(APIconstants.SOURCE) != null && !json.findValue(APIconstants.SOURCE).asText().isEmpty()) {
					sourceArea = json.findPath(APIconstants.SOURCE).textValue();

				}
				else {
					throw new Exception(APIconstants.SOURCE_ERROR);
				}
				if(json.findValue(APIconstants.DESTINATION) != null && !json.findValue(APIconstants.DESTINATION).asText().isEmpty()) {
					destArea = json.findPath(APIconstants.DESTINATION).textValue();	
				}
				else throw new Exception(APIconstants.DESTINATION_ERROR);

				if(json.findPath(APIconstants.SEATS_REQUESTED).asInt() != 0 && json.findValue(APIconstants.SEATS_REQUESTED).isInt()) {
					seats = json.findPath(APIconstants.SEATS_REQUESTED).intValue();
				}
				else {
					throw new Exception(APIconstants.SEATS_ERROR);
				}
				Timestamp timeOfTravel = null;
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				Date parsedDateFrom = dateFormat.parse(json.findPath(APIconstants.TRAVEL_TIME).textValue());

				Date today = new Date();
				if(parsedDateFrom.before(today)) {
					throw new Exception(APIconstants.TRAVEL_TIME_ERROR);
				}
				timeOfTravel = new java.sql.Timestamp(parsedDateFrom.getTime());

				try{
					RideRequest alreadyPlaced = RideRequest.getRideRequestById(session.userId);
					if(alreadyPlaced != null) {
						responseJson = RideRequest.updateRideRequest(session, sourceArea, destArea, timeOfTravel, seats);
						return ok(responseJson);
					}
					else {
						responseJson = RideRequest.PlaceRideRequest(session, sourceArea, destArea, timeOfTravel, seats);
						return ok(responseJson);
					}
				}
				catch(NullPointerException e) {
					return badRequest("Something went wrong!");
				}

			} 
			else {
				throw new Exception(APIconstants.SESSION_KEY_ERROR);
			}

		} 
		catch (Exception e) {
			return badRequest(e.getLocalizedMessage());
		}
	}

	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);
		return badRequest(result);
	}
}