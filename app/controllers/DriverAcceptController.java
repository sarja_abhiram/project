package controllers;

import models.Requests;
import models.Session;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class DriverAcceptController extends Controller{

	@CorsComposition.Cors
	public Result OnDriverAccept() {

		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);

		try {

			if(session != null) {

				JsonNode json = request().body().asJson();
				
				if(json == null) {
					return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
				}

				int rideRequestId = json.findPath(APIconstants.RIDE_REQUEST_ID).asInt();
				try{
					if(rideRequestId <= 0 || rideRequestId > 1000) {
						throw new Exception(APIconstants.RIDE_REQUEST_ID_INVALID);
					}
				}
				catch(Exception e) {
					return badRequest(e.getLocalizedMessage());
				}

				ObjectNode result = Requests.statusUpdateByDriver(session, rideRequestId);

				if(result == null) {
					return badRequest(APIconstants.RIDE_REQUEST_ID_NOT_FOUND);
				}
				return ok(result);
			}
			else {
				return badRequest(APIconstants.SESSION_KEY_ERROR);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest("Something went wrong");
		}

	}

	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);
		return ok(result);
	}

}
