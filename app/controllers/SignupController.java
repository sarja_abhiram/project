/**
 * 
 */
package controllers;

import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;
import Utils.APIvalidations;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
/**
 * @author fresher
 * 
 */

public class SignupController extends Controller {

	@CorsComposition.Cors
	public Result preflight(String path) {
		response().setHeader("Access-Control-Allow-Origin", "*");
		response().setHeader("Allow", "*");
		response().setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
		response().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Referer, User-Agent, sessionKey");
		System.out.println("got the options request");
		return ok("");
	}
    
	@CorsComposition.Cors
	public Result signUpUser() throws Exception{

		JsonNode json = request().body().asJson();

		String userName = null;
		String password =null;
		String email = null;
		String contact = null;
		
		if (json == null) {
			return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
		}
		try {
			if(json.findPath(APIconstants.USER_NAME).textValue() != null && !json.findValue(APIconstants.USER_NAME).textValue().isEmpty()) {
				userName = json.findPath(APIconstants.USER_NAME).textValue();
			}
			else throw new Exception(APIconstants.USERNAME_ERROR);
			
			
			if(json.findPath(APIconstants.PASSWORD).textValue() != null && !json.findValue(APIconstants.PASSWORD).textValue().isEmpty()) {
				password = json.findPath(APIconstants.PASSWORD).textValue();
			}
			else throw new Exception(APIconstants.PASSWORD_ERROR);
			
			if(json.findPath(APIconstants.EMAIL).textValue() != null && !json.findValue(APIconstants.EMAIL).textValue().isEmpty() 
					&& APIvalidations.isValidEmailAddress(json.findPath(APIconstants.EMAIL).textValue())) {
				
				email = json.findPath(APIconstants.EMAIL).textValue();
			}
			else throw new Exception(APIconstants.EMAIL_ERROR);
			
			if(json.findPath(APIconstants.CONTACT).textValue() != null && !json.findValue(APIconstants.CONTACT).textValue().isEmpty() 
					&& json.findPath(APIconstants.CONTACT).textValue().length() == 10 
					&& APIvalidations.isValidContactNumber(json.findPath(APIconstants.CONTACT).textValue())) {
				
				contact = json.findPath(APIconstants.CONTACT).textValue();
			}
			else throw new Exception(APIconstants.CONTACT_ERROR);
		}
		catch(Exception e) {
			return badRequest(e.getLocalizedMessage());
		}
		
		String result = User.createUser(userName, password, email, contact);
		ObjectNode response = Json.newObject();
		
		if(result == APIconstants.SUCCESS) {
			return ok(response.put("status", result));
//			return ok(result);
		}
		
		else if(result == APIconstants.USERNAME_EXISTS_ERROR) {
			return ok(response.put("status", result));
		}
		return badRequest("Something went wrong!");
	}

	
	
	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);

		return ok(result);
	}

}