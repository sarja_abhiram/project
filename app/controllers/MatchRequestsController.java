package controllers;

import models.RideRequest;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;

public class MatchRequestsController extends Controller{


	@CorsComposition.Cors
	public Result MatchRequest() {

		try {
			String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
			Session session = Session.getSession(sessionKey);
			if(session != null) {
				JsonNode result = RideRequest.matchRequests(session);
				if(result == null) {
					return badRequest(APIconstants.NO_DRIVERS);
				}
				return ok(result);
			}
			else throw new Exception(APIconstants.SESSION_KEY_ERROR);
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest("");
		}

	}
}