package controllers;

import models.Requests;
import models.RideRequest;
import models.Session;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class PassengerRequestController extends Controller{

	@CorsComposition.Cors
	public Result OnPassengerRequest() {

		JsonNode json = request().body().asJson();

		try {
			if (json == null) {
				return returnErrorJSONResponse("Invalid Data");
			}
			String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
			int rideId = json.findPath(APIconstants.RIDE_ID).asInt();

			Session session = Session.getSession(sessionKey);
			if(session != null) {
				int userId = session.userId;
				RideRequest rideRequest = RideRequest.getRideRequestById(userId);
				int rideRequestId = rideRequest.rideRequestId;

				if(Requests.UpdateRequest(rideRequestId, rideId)) {
					return ok();
				}
				else return badRequest(APIconstants.NO_REQUEST);
			}
			else return badRequest(APIconstants.SESSION_KEY_ERROR);	
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest("something went wrong!");
		}

	}

	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);
		return ok(result);
	}
}
