package controllers;

import models.Requests;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;

public class TakeRideController extends Controller{

	@CorsComposition.Cors
	public Result onTakeRide() {

		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		String result = null;
		
		if(session != null) {
			
			result = Requests.isRideExists(session);
			
			if(result == APIconstants.RIDE_EXISTS_TODAY) {
				return badRequest(APIconstants.RIDE_EXISTS_TODAY);
			}
			if(result == APIconstants.SUCCESS){
				return ok(APIconstants.SUCCESS);
			}
			else return ok(APIconstants.SUCCESS);
		}

		else return badRequest(APIconstants.SESSION_KEY_ERROR);	

	}
	
	@CorsComposition.Cors
	public Result onTakeRideAgain() {

		String result = null;
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);

		if(session != null) {

			try{
				result = Requests.onTakeRideAgain(session);
				if(result == APIconstants.ALREADY_ON_TRIP) {
					return badRequest(APIconstants.ALREADY_ON_TRIP);
				}

				if(result == APIconstants.SUCCESS) {
					return ok(APIconstants.SUCCESS);
				}
				
				if(result == null) {
					return badRequest();
				}
			}
			catch(Exception e) {
				return badRequest();
			}			
		}
		else return badRequest(APIconstants.SESSION_KEY_ERROR);

		return badRequest();
	}

}


//List<UserCar> userCarListByUser = UserCar.getUserCarByUserId(session.userId);
//Ride ride = new Ride();
//
//for(UserCar usercaritem : userCarListByUser) {
//	
//	ride = Ebean.find(Ride.class).where().eq("user_car_id", usercaritem.userCarId).findUnique();
//	
//	java.util.GregorianCalendar rideDate = new java.util.GregorianCalendar();
//	rideDate.setTime(new Date(ride.travelTime.getTime()));
//	java.util.GregorianCalendar today = new java.util.GregorianCalendar();
//
//	if(ride != null) {
//		if(new DateTime(today).getDayOfMonth() == new DateTime(rideDate).getDayOfMonth())
//		return badRequest(APIconstants.RIDE_EXISTS_TODAY);
//		else return ok(APIconstants.SUCCESS);
//	}
//}
//return ok(APIconstants.SUCCESS);