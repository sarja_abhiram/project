package controllers;

import models.Session;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class LoginController extends Controller{

	@CorsComposition.Cors
	public Result loginUser() throws Exception {

		JsonNode json = request().body().asJson();
		ObjectNode result = Json.newObject();
		String userName = null;
		String password = null;

		if(json == null) {
			return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
		}
		
		try{
			if(json.findPath(APIconstants.USER_NAME).textValue() != null && !json.findValue(APIconstants.USER_NAME).textValue().isEmpty()) {
				userName = json.findPath(APIconstants.USER_NAME).textValue();
			}
			else throw new Exception(APIconstants.USERNAME_ERROR);

			if(json.findPath(APIconstants.PASSWORD).textValue() != null && !json.findValue(APIconstants.PASSWORD).textValue().isEmpty()) {
				password = json.findPath(APIconstants.PASSWORD).textValue();
			}
			else throw new Exception(APIconstants.PASSWORD_ERROR);
		}
		catch(Exception e) {
			e.getLocalizedMessage();
			return badRequest(e.getLocalizedMessage());
		}
		
		if(!Session.isSessionExists(userName)) {
			try {
				result = User.validateUsernameAndPassword(userName, password);
			}
			catch(Exception e) {	
				return badRequest(APIconstants.USERNAME_NOT_EXISTS);
			}
		}
		else return badRequest(APIconstants.SESSION_EXISTS);
		return ok(result);
	}


	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);
		return ok(result);
	}

}
