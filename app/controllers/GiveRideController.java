package controllers;

import models.Requests;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;

public class GiveRideController extends Controller {

	@CorsComposition.Cors
	public Result onGiveRide() {

		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		String result = null;

		if (session != null) {

			result = Requests.onGiveRide(session);

			if(result == APIconstants.RIDE_REQUEST_EXISTS_TODAY) {
				return badRequest(APIconstants.RIDE_REQUEST_EXISTS_TODAY);
			}

			if(result == APIconstants.SUCCESS) {
				return ok(APIconstants.SUCCESS);
			}
		}
		else {
			return badRequest(APIconstants.SESSION_KEY_ERROR);
		}
		return badRequest();
	}

}
