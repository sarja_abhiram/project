package controllers;

import models.Requests;
import models.Session;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;


public class DriverFinalViewController extends Controller{

	@CorsComposition.Cors
	public Result whoAccepted() {

		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		
		JsonNode result = Json.newObject();
		
		try {
			if(session != null) {
				result = Requests.getMyPassengers(session);
				if(result == null) {
					return ok(APIconstants.NO_PASSENGERS);
				}
				return ok(result);
			}
			else return badRequest(APIconstants.SESSION_KEY_ERROR);
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest("Something went wrong!");
		}
	}
}
