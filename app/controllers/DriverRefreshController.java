package controllers;

import models.Session;
import models.User;
import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Result;


public class DriverRefreshController extends Controller{
	
	@CorsComposition.Cors
	public Result onDriverRefresh() {
		
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		
		try{
			if(session != null) {
				JsonNode passengers = User.findPassengers(session);
				if(passengers.size() == 0) {
					return ok(APIconstants.NO_DATA);
				}
				else return ok(passengers);
			}
			else return badRequest(APIconstants.SESSION_KEY_ERROR);
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest();
		}
	}
	
	@CorsComposition.Cors
	public Result onDriverCancel() {
		
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		
		try{
			if(session != null) {
				JsonNode passengers = User.finalListOfPassengers(session);
				return ok(passengers);
			}
			else throw new Exception(APIconstants.SESSION_KEY_ERROR);
		}
		catch(Exception e) {
			e.printStackTrace();
			return badRequest();
		}
	}
	
}
