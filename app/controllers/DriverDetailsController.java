package controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.Ride;
import models.Session;
import models.User;
import models.UserCar;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class DriverDetailsController extends Controller {

	@CorsComposition.Cors
	public Result driverDetails() throws Exception{

		JsonNode json = request().body().asJson();
		
		ObjectNode result = Json.newObject();
		
	try {
			if (json == null) {
				return returnErrorJSONResponse(APIconstants.JSON_DATA_ERROR);
			}
			String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
			Session session = Session.getSession(sessionKey);
			
			String carName = null;
			String sourceArea = null;
			String destArea = null;
			int seatsAvailable = 0;
				
			if(session != null) {
				if(json.findPath(APIconstants.CAR_NAME).textValue() != null && !json.findValue(APIconstants.CAR_NAME).textValue().isEmpty()) {
					carName = json.findPath(APIconstants.CAR_NAME).textValue();
				}
				else {
					throw new Exception(APIconstants.CAR_NAME_ERROR);
				}
				
				if(json.findPath(APIconstants.SOURCE).textValue() != null && !json.findPath(APIconstants.SOURCE).textValue().isEmpty()) {
					sourceArea = json.findPath(APIconstants.SOURCE).textValue();
					
				}
				else {
					throw new Exception(APIconstants.SOURCE_ERROR);
				}
				if(json.findPath(APIconstants.DESTINATION).textValue() != null && !json.findPath(APIconstants.DESTINATION).textValue().isEmpty()) {
					destArea = json.findPath(APIconstants.DESTINATION).textValue();	
				}
				else throw new Exception(APIconstants.DESTINATION_ERROR);
				
				seatsAvailable = json.findPath(APIconstants.SEATS_REQUESTED).intValue();
				if(seatsAvailable <= 0){
					throw new Exception(APIconstants.SEATS_ERROR);
				}
				
				Timestamp timeOfTravel = null;
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				Date parsedDateFrom = dateFormat.parse(json.findPath(APIconstants.TRAVEL_TIME).textValue());
				System.out.println(parsedDateFrom);
				timeOfTravel = new java.sql.Timestamp(parsedDateFrom.getTime());
				
				JsonNode via = json.get(APIconstants.VIA);
				String[] viaAreas = new String[via.size()];
				if(via.size() == 0) {
					throw new Exception(APIconstants.VIA_ERROR);
				}
				
				for(int i=0; i< via.size();i++) {
					viaAreas[i] = via.path(i).findPath(APIconstants.AREA_NAME).asText();
				}
				
				double costPerHead = json.findPath(APIconstants.COST_PER_HEAD).asDouble();
				if(costPerHead <= 0.0 ) {
					throw new Exception(APIconstants.COST_PER_HEAD_ERROR);
				}
				UserCar usercar = UserCar.getUserCarByUserIdCarName(session.userId, carName);
				List<UserCar> userCarListByUser = UserCar.getUserCarByUserId(session.userId);
				
				try{
					Ride newride = Ride.getRideByUserCarId(usercar.userCarId);
					if(newride != null) {
						result = User.UpdateDriverDetails(session, usercar.userCarId, sourceArea, destArea, timeOfTravel, 
								viaAreas,seatsAvailable, costPerHead);
						return ok(result);
					}
					else {
						Ride ride = new Ride();
						for(UserCar usercaritem : userCarListByUser) {
							ride = Ebean.find(Ride.class).where().eq("user_car_id", usercaritem.userCarId).findUnique();
							if(ride != null) {
							result = Ride.updateExistingRideByUser(session, ride.rideId, usercar.userCarId, sourceArea, destArea, timeOfTravel, 
									viaAreas, seatsAvailable,costPerHead);
							return ok(result);
							}
						}
					result = User.addDriverDetails(session, usercar.userCarId, sourceArea, destArea, timeOfTravel, 
									viaAreas, seatsAvailable,costPerHead);
					return ok(result);
						
					}
				}
				catch(Exception e) {
					e.printStackTrace();
					return badRequest(e.getLocalizedMessage());
				}
				
			} 
			else {
				return returnErrorJSONResponse(APIconstants.SESSION_KEY_ERROR);
			}

	} 
	catch (Exception e) {
		System.out.println(e);
		return badRequest(e.getLocalizedMessage());
	}
}

	private Result returnErrorJSONResponse(String msg) {
		ObjectNode result = Json.newObject();
		result.put("Error", msg);

		return ok(result);
	}
}
