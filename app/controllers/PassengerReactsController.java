package controllers;

import java.util.List;

import models.Requests;
import models.RideRequest;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;

public class PassengerReactsController extends Controller{

	@CorsComposition.Cors
	public Result cancelRequest() {
		
		String sessionKey = request().getHeader("sessionKey");
		Session session = Session.getSession(sessionKey);
		
		JsonNode json = request().body().asJson();
	try{
		if(session != null) {
			int rideId = json.findPath("rideId").asInt();
			
			RideRequest rideRequest = Ebean.find(RideRequest.class).where().eq("user_id", session.userId).findUnique();
			
			List<Requests> requestList = Ebean.find(Requests.class).where()
					.eq("ride_request_id", rideRequest.rideRequestId).findList();
			
			for(Requests request : requestList) {
				if(request.rideId == rideId) {
					
					SqlUpdate update = Ebean
							.createSqlUpdate("UPDATE requests set user_status= :param1 "
									+ "where request_id= :param2 and driver_status = :param3");

					update.setParameter("param1", "rejected");
					update.setParameter("param2", request.requestId);
					update.setParameter("param3", "Accepted");
					update.execute();
					return ok("success");
				}
			}
		}
		else return badRequest("Session not found");
	}
	catch(Exception e) {
		e.printStackTrace();
		return badRequest();
	}
	return badRequest();
	}
	
	@CorsComposition.Cors
	public Result acceptRequest() {
		
		String sessionKey = request().getHeader("sessionKey");
		Session session = Session.getSession(sessionKey);
		
		JsonNode json = request().body().asJson();
	try{
		if(session != null) {
			int rideId = json.findPath("rideId").asInt();
			
			RideRequest rideRequest = Ebean.find(RideRequest.class).where().eq("user_id", session.userId).findUnique();
			
			List<Requests> requestList = Ebean.find(Requests.class).where()
					.eq("ride_request_id", rideRequest.rideRequestId).findList();
			
			for(Requests request : requestList) {
				if(request.rideId == rideId) {
					
					SqlUpdate update = Ebean
							.createSqlUpdate("UPDATE requests set user_status= :param1 "
									+ "where request_id= :param2 and driver_status = :param3");

					update.setParameter("param1", "Accepted");
					update.setParameter("param2", request.requestId);
					update.setParameter("param3", "Accepted");
					update.execute();
					return ok("success");
				}
			}
		}
		else return badRequest("Session not found");
	}
	catch(Exception e) {
		e.printStackTrace();
		return badRequest();
	}
	return badRequest();
	}
}
