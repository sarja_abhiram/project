package controllers;


import models.Ride;
import models.Session;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import Utils.APIconstants;

import com.fasterxml.jackson.databind.node.ObjectNode;


public class LoadDriverDetailsController extends Controller{

	@CorsComposition.Cors
	public Result onLoadingDriver() throws Exception{
		
		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
		ObjectNode result = Json.newObject();
		
		if(session != null) {
			try{
				result = Ride.loadRideDetails(session);
				if(result == null) {
					return ok(APIconstants.NO_DATA);
				}
				return ok(result);
			}
			catch(Exception e) {
				return badRequest(APIconstants.RIDE_ID_NOT_FOUND);
			}
		}
		else {
			return badRequest(APIconstants.SESSION_KEY_ERROR);
		}
	}
}
