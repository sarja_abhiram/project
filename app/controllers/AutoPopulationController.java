package controllers;

import models.Area;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;
import Utils.APIconstants;

import com.fasterxml.jackson.databind.JsonNode;

public class AutoPopulationController extends Controller{

	@CorsComposition.Cors
	public Result autoPopulation() {

		String sessionKey = request().getHeader(APIconstants.SESSION_KEY);
		Session session = Session.getSession(sessionKey);
	
			if(session != null) {
				JsonNode json = request().body().asJson();
				String areaEntered = json.findPath(APIconstants.AREA_NAME).textValue();
				if(areaEntered == "")
					return badRequest(APIconstants.AREA_NAME_ERROR);
				JsonNode areas = Area.autoPopulation(areaEntered);
//				System.out.println(areas.toString());	
				if(areas.size() == 0) {
					return ok(APIconstants.AREA_NOT_FOUND);
				}
				return ok(areas);
			}
			else return badRequest(APIconstants.SESSION_KEY_ERROR);
	}

}