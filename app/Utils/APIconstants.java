package Utils;

public class APIconstants {

	public static final String JSON_DATA_ERROR = "Expecting JSON data";
	
	public static final String SESSION_KEY = "sessionKey";
	
	public static final String SESSION_KEY_ERROR = "Invalid session key!";
	
	public static final String SESSION_EXISTS = "Session exists already!";
	
	public static final String SESSION_NOT_EXISTS = "Session does not exists!";
	
/* 
 * 
 * API constants for sign-up and sign-in
 * 
 */	
	public static final String USER_NAME = "userName";
	
	public static final String PASSWORD = "password";
	
	public static final String EMAIL = "email";
	
	public static final String CONTACT = "contact";
	
	public static final String USERNAME_ERROR = "Invalid user name";
	
	public static final String USERNAME_EXISTS_ERROR = "User name exists";
	
	public static final String USERNAME_NOT_EXISTS = "User name Not found!";
	
	public static final String PASSWORD_ERROR = "Invalid password";
	
	public static final String EMAIL_ERROR = "Invalid email id";
	
	public static final String CONTACT_ERROR = "Invalid conatct info";
/* 
 * 
 * API constants for ride-request and driver-details
 * 
 */	
	public static final String SOURCE = "from";
	
	public static final String VIA = "via";
	
	public static final String DESTINATION = "to";
	
	public static final String TRAVEL_TIME = "time";
	
	public static final String SEATS_REQUESTED = "seats";
	
	public static final String AREA_NAME = "areaName";
	
	public static final String COST_PER_HEAD = "costPerHead";
	
	public static final String CAR_NAME = "carName";
	
	public static final String AREA_NAME_ERROR = "Invalid area name!";
	
	public static final String AREA_NOT_FOUND = "No areas found";
	
	public static final String SOURCE_ERROR = "Source Area required";
	
	public static final String VIA_ERROR = "Via routes missing!";
	
	public static final String DESTINATION_ERROR = "Destination Area required";
	
	public static final String SEATS_ERROR = "Invalid data for seats";
	
	public static final String TRAVEL_TIME_ERROR = "Invalid travel time!";
	
	public static final String COST_PER_HEAD_ERROR = "Enter valid cost-per-head!";
	
	public static final String CAR_NAME_ERROR = "Car Name required";
	
	public static final String DRIVER_LICENCE = "dlNumber";
	
	public static final String COMFORT_LEVEL = "comfortLevel";
	
	public static final String DRIVER_LICENCE_INVALID = "Invalid input for Driver licence";
	
	public static final String COMFORT_LEVEL_INVALID = "Invalid input for Comfort Level";
/*
 * 
 * API constants for DriverAcceptController	
*/	
	public static final String RIDE_REQUEST_ID = "rideRequestId";
	
	public static final String RIDE_ID = "rideId";
	
	public static final String RIDE_REQUEST_ID_INVALID ="Invalid entry for ride request id";
	
	public static final String RIDE_REQUEST_ID_NOT_FOUND = "ride request id not found!";
	
	public static final String RIDE_ID_NOT_FOUND = "ride not found!";
	
	public static final String NO_PASSENGERS = "no passengers";
	
	public static final String NO_DRIVERS = "no drivers";
	
	public static final String NO_DATA = "no data";
	
	public static final String NO_REQUEST = "Record not found";
	
	public static final String RIDE_EXISTS_TODAY = "Ride already exists for today!";
	
	public static final String RIDE_REQUEST_EXISTS_TODAY = "ride request is already present for today!";
	
	public static final String ALREADY_ON_TRIP = "YOU ARE ALREADY ON TRIP";
	
	public static final String SUCCESS = "success";
	
	public static final String USER_STATUS_MATCHED = "matched";
	
	public static final String USER_STATUS_REQUESTED = "requested";
	
	public static final String DRIVER_STATUS_MATCHED = "matched";
	
	public static final String DRIVER_STATUS_ACCEPTED = "Accepted";
}