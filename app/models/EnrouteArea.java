package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;


@Entity
public class EnrouteArea extends Model{
	
	@Id
	public int enrouteId;
	
	@ManyToOne
	@JoinColumn(table="Area")
	public int enrouteAreaId;
	
	@ManyToOne
	@JoinColumn(table="Ride", referencedColumnName="rideId")
	public int rideId;
	
	public static Finder<String, EnrouteArea> find = new Finder<String,EnrouteArea>(EnrouteArea.class);
	
	public static void createEnrouteAreas(int areaId, int rideId) {
		EnrouteArea enrouteArea = new EnrouteArea();
		enrouteArea.enrouteAreaId = areaId;
		enrouteArea.rideId = rideId;
		Ebean.save(enrouteArea);
		
	}
	
	public static void deleteEnrouteAreas(int rideId) {
		SqlUpdate deleteAreas = Ebean.createSqlUpdate("DELETE FROM enroute_area WHERE ride_id = :param1");
		deleteAreas.setParameter("param1", rideId);
		deleteAreas.execute();
	}
	
	public static List<EnrouteArea> getEnrouteAreasByRideId(int rideId) {
		
		List<EnrouteArea> enrouteAreasList = Ebean.find(EnrouteArea.class).where().eq("ride_id", rideId).findList();
		return enrouteAreasList;
		
	}
}
