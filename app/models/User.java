package models;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import play.libs.Json;

import Utils.APIconstants;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Entity
public class User extends Model {

	@Id
	public int userId;

	public String userName;

	@NotNull
	public String password;

	@NotNull
	public String email;

	@NotNull
	public String contact;

	public String DLnumber;

	public static Finder<String, User> find = new Finder<String, User>(
			User.class);



	public static String createUser(String userName, String password, String email, String contact) {
		
		User user = Ebean.find(User.class).where().eq("user_name", userName).findUnique();
		
		if(user != null) {
			return APIconstants.USERNAME_EXISTS_ERROR;
		}
		else {
			User newUser = new User();
			newUser.userName = userName;
			newUser.password = password;
			newUser.email = email;
			newUser.contact = contact;
			Ebean.save(newUser);
			return APIconstants.SUCCESS;
		}
		
	}

	public static User getUserId(int userId) {
		User user = Ebean.find(User.class).where().eq("user_id", userId)
				.findUnique();
		return user;
	}

	public static User getUserByName(String name) {
		User user = Ebean.find(User.class).where().eq("user_name", name).findUnique();
		return user;
	}

	public static ObjectNode validateUsernameAndPassword(String userName, String password) throws Exception {
		ObjectNode result = Json.newObject();
		User tempUser = new User();
		
		tempUser = Ebean.find(User.class).where().eq("user_name", userName).findUnique();
		
		if(tempUser != null){
		
			if(tempUser.password.equals(password)) {
				String sessionKey = Session.generateSessionKey();
				Session.createSession(sessionKey, tempUser.userName, tempUser.userId);
				
				result.put("status", "login-successful");
				result.put("username", "found");
				result.put("password", "correct");
				result.put("sessionKey", sessionKey);
				return result;
			}
		else {
				result.put("status", "login-unsuccessful");
				result.put("username", "found");
				result.put("password", "incorrect");
				return result;
			}
			
		}
		else{
			throw new Exception("Invalid UserName");
		}	
	}
	
	public static boolean validateUsernameAndPasswordAgain(String userName, String password) throws Exception {
		
		User tempUser = new User();
		tempUser = Ebean.find(User.class).where().eq("user_name", userName).findUnique();
		
		if(tempUser != null){
			
			if(tempUser.password.equals(password)) {
				return true;
			}
		else {
				return false;
			}
		}
		else throw new Exception(APIconstants.USERNAME_NOT_EXISTS);
	}

	public static void updateUserDrivingLicence(String dlNumber, int userId) {

		SqlUpdate update = Ebean
				.createSqlUpdate("UPDATE user set dlNumber= :param1 "
						+ "where user_id= :param2");
		update.setParameter("param1", dlNumber);
		update.setParameter("param2", userId);
		update.execute();
	}


	public static ObjectNode addDriverDetails(Session session, int userCarId, String sourceArea, String destArea, Timestamp time, String[] viaAreas, 
			int seatsAvailable, Double costPerHead) {

		ObjectNode result = Json.newObject();
		int areaId = 0;
		
	try {
		
		int sourceAreaId = Area.getAreaId(sourceArea);
		int destinationAreaId = Area.getAreaId(destArea);

		Ride ride = Ride.CreateRide(userCarId, sourceAreaId, destinationAreaId, time, seatsAvailable, costPerHead);

		for(String areaname : viaAreas) {
			areaId= Area.getAreaId(areaname);
			EnrouteArea.createEnrouteAreas(areaId, ride.rideId);
		}

		result.put("sessionKey", session.sessionId);
		result.put("status", "success");

		return result;
		}
	
	catch(Exception e){
		result.put("status", "Unsuccess");
		e.printStackTrace();
		return result;
	}
}
	
	public static ObjectNode UpdateDriverDetails(Session session, int userCarId, String sourceArea, String destArea, 
			Timestamp time, String[] viaAreas, int seatsAvailable, Double costPerHead) {

		ObjectNode result = Json.newObject();
		int areaId = 0;
		
	try {
		
		int sourceAreaId = Area.getAreaId(sourceArea);
		int destinationAreaId = Area.getAreaId(destArea);

		Ride ride = Ride.UpdateRide(userCarId, sourceAreaId, destinationAreaId, time, seatsAvailable, costPerHead);
		
		EnrouteArea.deleteEnrouteAreas(ride.rideId);
		
		for(String areaname : viaAreas) {
			areaId= Area.getAreaId(areaname);
			EnrouteArea.createEnrouteAreas(areaId, ride.rideId);
		}

		result.put("sessionKey", session.sessionId);
		result.put("status", "success");

		return result;
		}
	catch(Exception e){
		
		result.put("status", "Unsuccess");
		e.printStackTrace();
		return result;
	}
}

	public static JsonNode findPassengers(Session session) {

	try{
		String query = "select u.user_id, u.user_name, u.email, u.contact, rq.ride_request_id, rq.travel_time " +
				"from user u, ride_request rq where u.user_id = rq.user_id and " +
				"rq.ride_request_id IN " +
				"(select ride_request_id from requests where ride_id = " +
				"(select r.ride_id from user_car uc, ride r where uc.user_id =" + session.userId + 
				" and uc.user_car_id = r.user_car_id) and user_status = 'requested' and driver_status = 'matched')";
		
		SqlQuery queryObj = Ebean.createSqlQuery(query);
		List<SqlRow> passengersList = queryObj.findList();
		JsonNode passengersJson = Json.toJson(passengersList);
		return passengersJson;
	}
	catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}

	public static JsonNode finalListOfPassengers(Session session) {

	try{
		String query = "select u.user_id, u.user_name, u.email, u.contact, rq.ride_request_id, rq.travel_time " +
				"from user u, ride_request rq where u.user_id = rq.user_id and " +
				"rq.ride_request_id IN " +
				"(select ride_request_id from requests where ride_id = " +
				"(select r.ride_id from user_car uc, ride r where uc.user_id =" + session.userId + 
				" and uc.user_car_id = r.user_car_id) and user_status = 'Accepted' and driver_status = 'Accepted')";
		
		SqlQuery queryObj = Ebean.createSqlQuery(query);
		List<SqlRow> passengersList = queryObj.findList();
		JsonNode passengersJson = Json.toJson(passengersList);
		return passengersJson;
	}
	catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}


}
