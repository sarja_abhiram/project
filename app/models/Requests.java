package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.joda.time.DateTime;

import play.libs.Json;

import Utils.APIconstants;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Entity
public class Requests extends Model{

	@Id
	public int requestId;

	@ManyToOne
	@JoinColumn(table="Ride")
	public int rideId;

	@ManyToOne
	@JoinColumn(table="RideRequest")
	public int rideRequestId;

	public String userStatus;

	public String driverStatus;


	public static Finder<String, Requests> find = new Finder<String,Requests>(Requests.class);


	public static List<Requests> getRequestsByRideId(int rideId) {
		List<Requests> requestsForRide = Ebean.find(Requests.class).where().eq("ride_id",rideId).findList();
		return requestsForRide;
	}

	public static List<Requests> getRequestsByRequestId(int rideRequestId) {
		List<Requests> requestsForRide = Ebean.find(Requests.class).where().eq("ride_request_id",rideRequestId).findList();
		return requestsForRide;
	}

	public static boolean UpdateRequest(int rideRequestId, int rideId) {

		try {
			SqlUpdate update = Ebean
					.createSqlUpdate("UPDATE requests set user_status= :param1 "
							+ "where ride_request_id= :param2 and ride_id = :param3");
			update.setParameter("param1", "requested");
			update.setParameter("param2", rideRequestId);
			update.setParameter("param3", rideId);
			update.execute();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static JsonNode notifyPassenger(Session session) {

		RideRequest rideRequest = Ebean.find(RideRequest.class).where().eq("user_id", session.userId).findUnique();
		Requests request = Ebean.find(Requests.class).where().eq("ride_request_id", rideRequest.rideRequestId)
				.eq("driver_status", "Accepted").findUnique();

		String query = "select u.user_id, u.user_name, u.contact from user u where u.user_id IN " +
				"(select user_id from user_car where user_id =" +
				" (select user_id from ride where ride_id =" + request.rideId +"))";

		SqlQuery queryObj = Ebean.createSqlQuery(query);
		SqlRow driver = queryObj.findUnique();
		JsonNode jsonDriver = Json.toJson(driver);
		return jsonDriver;
	}


	public static ObjectNode statusUpdateByDriver(Session session, int rideRequestId) {

		ObjectNode result = Json.newObject();
		try{
			String query = "select r.ride_id " +
					"from ride r, user_car uc where " +
					"uc.user_car_id = r.user_car_id and uc.user_id = "+ session.userId;
			SqlQuery queryObj = Ebean.createSqlQuery(query);
			SqlRow rideId = queryObj.findUnique();

			Requests requests = Ebean.find(Requests.class).where().eq("ride_id", rideId.get("ride_id"))
					.eq("ride_request_id", rideRequestId)
					.eq("user_status", "requested").findUnique();
			if(requests != null) {
				SqlUpdate update = Ebean
						.createSqlUpdate("UPDATE requests set driver_status= :param1 "
								+ "where request_id= :param2");

				update.setParameter("param1", "Accepted");
				update.setParameter("param2", requests.requestId);
				update.execute();


				SqlUpdate update2 = Ebean
						.createSqlUpdate("UPDATE requests set user_status= :param1 "
								+ "where ride_request_id= :param2 and driver_status = :param3");

				update2.setParameter("param1", "NA");
				update2.setParameter("param2", rideRequestId);
				update2.setParameter("param3", "matched");
				update2.execute();


				result.put("Status", "success");
				return result;

			}
			else {
				result.put("status", "User is accepted by others!");
				return result;
			}


		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}

	}


	public static boolean isRequestPresent(int rideRequestId, int rideId) {

		try{
			List<Requests> requestList = Ebean.find(Requests.class).where().eq("ride_request_id", rideRequestId).findList();

			for(Requests request : requestList) {
				if(request.rideId == rideId)
					return true;
			}
			return false;
		}
		catch(Exception e ) {
			return false;
		}
	}

	public static JsonNode whoIsMyDriver(Session session) {

		try{
			RideRequest rideRequest = RideRequest.getRideRequestById(session.userId);
			String query = "select u.user_id, u.user_name, u.contact, uc.car_name, uc.comfort_lavel, r.travel_time, r.ride_id, r.cost_per_head " +
					"from user u, ride r, user_car uc where u.user_id = uc.user_id and " +
					"uc.user_car_id = r.user_car_id and r.ride_id IN (select ride_id from requests " +
					"where ride_request_id ="+ rideRequest.rideRequestId +" and user_status = 'requested' " +
					"and driver_status = 'Accepted')";

			SqlQuery queryObj = Ebean.createSqlQuery(query);
			List<SqlRow> driversList = queryObj.findList();
			JsonNode driversJson = Json.toJson(driversList);
			return driversJson;
		}	
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static JsonNode getMyPassengers(Session session) {

		JsonNode jsonList = Json.newObject();
		List<RideRequest> rideRequestList = new ArrayList<RideRequest>();
		List<User> passengerList = new ArrayList<User>();

		try{
			String query = "select r.ride_id " +
					"from ride r, user_car uc where " +
					"uc.user_car_id = r.user_car_id and uc.user_id = "+ session.userId;
			SqlQuery queryObj = Ebean.createSqlQuery(query);
			SqlRow rideId = queryObj.findUnique();

			List<Requests> requestsList = Ebean.find(Requests.class).where().eq("ride_id", rideId.get("ride_id"))
					.eq("driver_status", APIconstants.DRIVER_STATUS_ACCEPTED)
					.eq("user_status", APIconstants.USER_STATUS_REQUESTED).findList();

			RideRequest rideRequest = new RideRequest();
			for(Requests request : requestsList) {
				rideRequest = RideRequest.getRideRequest(request.rideRequestId);
				rideRequestList.add(rideRequest);
			}

			User user = new User();
			for(RideRequest riderequest : rideRequestList) {
				user = User.getUserId(riderequest.userId);
				passengerList.add(user);
			}
			jsonList =  Json.toJson(passengerList);
			return jsonList;
		}
		catch(Exception e) {
			//		e.printStackTrace();
			return null;
		}
	}

	public static String onTakeRideAgain(Session session) {

		String result = null;

		Date today = new Date();
		User newUser = User.getUserByName(session.userName);
		RideRequest rideRequest = RideRequest.getRideRequestById(newUser.userId);

		List<Requests> requestList = Ebean.find(Requests.class).where()
				.eq("ride_request_id", rideRequest.rideRequestId).findList();

		java.util.GregorianCalendar rideDate = new java.util.GregorianCalendar();
		rideDate.setTime(new Date(rideRequest.travelTime.getTime()));

		int hoursLeft = 23 - new DateTime(rideDate).getHourOfDay();
		int minsLeft = 60 - new DateTime(rideDate).getMinuteOfHour();
		int minsLeft2 = minsLeft + hoursLeft * 60;
		int SEC_LEFT = 60 * minsLeft2;

		Date nextDay = new Date(rideRequest.travelTime.getTime()/1000 + SEC_LEFT);

		if(rideRequest.travelTime.getTime()/1000 < nextDay.getTime() 
				&& new DateTime(rideDate).getDayOfMonth() <= new DateTime(today).getDayOfMonth()) {
			for(Requests request : requestList) {
				if(request.userStatus.equals(APIconstants.USER_STATUS_REQUESTED) && request.driverStatus.equals(APIconstants.DRIVER_STATUS_ACCEPTED)) {
					return (APIconstants.ALREADY_ON_TRIP);
				}
			}
		}
		else return (APIconstants.SUCCESS);

		return result;
	}

	public static String onGiveRide(Session session) {

		RideRequest rideRequest = Ebean.find(RideRequest.class).where()
				.eq("user_id", session.userId).findUnique();

		if (rideRequest != null) {

			java.util.GregorianCalendar rideDate = new java.util.GregorianCalendar();
			rideDate.setTime(new Date(rideRequest.travelTime.getTime()));

			java.util.GregorianCalendar today = new java.util.GregorianCalendar();

			int hoursLeft = 23 - new DateTime(rideDate).getHourOfDay();
			int minsLeft = 60 - new DateTime(rideDate).getMinuteOfHour();							
			int minsLeft2 = minsLeft + hoursLeft * 60;
			int SEC_LEFT = 60 * minsLeft2;

			Date nextDay = new Date(rideRequest.travelTime.getTime()/1000 + SEC_LEFT);

			if (rideRequest.travelTime.getTime()/1000 < nextDay.getTime() 
					&& new DateTime(rideDate).getDayOfMonth() <= new DateTime(today).getDayOfMonth()) {
				return (APIconstants.RIDE_REQUEST_EXISTS_TODAY);
			} 
			else return (APIconstants.SUCCESS);
		} 
		else return (APIconstants.SUCCESS);
	}



	public static String isRideExists(Session session) {

		List<UserCar> userCarListByUser = UserCar.getUserCarByUserId(session.userId);
		Ride ride = new Ride();

		for(UserCar usercaritem : userCarListByUser) {

			try{
				ride = Ebean.find(Ride.class).where().eq("user_car_id", usercaritem.userCarId).findUnique();
				
				java.util.GregorianCalendar rideDate = new java.util.GregorianCalendar();
				rideDate.setTime(new Date(ride.travelTime.getTime()));
				java.util.GregorianCalendar today = new java.util.GregorianCalendar();

				if(ride != null) {
					if(new DateTime(today).getDayOfMonth() == new DateTime(rideDate).getDayOfMonth())
						return (APIconstants.RIDE_EXISTS_TODAY);
					else return (APIconstants.SUCCESS);
				}

			}
			catch(Exception e) {
				return (APIconstants.SUCCESS);
			}
		}
		return (APIconstants.SUCCESS);

	}

}
