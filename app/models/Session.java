package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import play.data.validation.Constraints.Required;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;

@Entity
public class Session extends Model{


	public String userName;

	@Id
	@OneToOne
	@JoinColumn(table="User", referencedColumnName="userId")
	public int userId;

	@Required
	public String sessionId;

	public static Finder<String, Session> find = new Finder<String,Session>(Session.class);

	public static Session createSession(String sessionId, String username, int userId) {
		Session session = new Session();
		session.sessionId = sessionId;
		session.userName = username;
		session.userId = userId;
		Ebean.save(session);
		return session;
	}

	public static String getUsername(String sessionToken) {
		Session session = Ebean.find(Session.class).where().eq("session_id", sessionToken).findUnique();
		return session.userName;
	}

	public static Session getSession(String sessionToken) {
		Session session = Ebean.find(Session.class).where().eq("session_id", sessionToken).findUnique();
		return session;
	}

	public static String generateSessionKey() {
		String sessionKey = UUID.randomUUID().toString().replaceAll("-", "");
		return sessionKey;
	}
	
	public static boolean isSessionExists(String userName) {
		Session session = Ebean.find(Session.class).where().eq("user_name", userName).findUnique();
		
		if(session == null) {
			return false;
		}
		else return true;
	}
	
}
