package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;

@Entity
public class UserCar extends Model {

	@Id
	public int userCarId;
	
	@ManyToOne
	@JoinColumn(table="User", referencedColumnName="userId")
	public int userId;
	
	public String carName;
	
	public int comfortLavel;
	
	public static Finder<String, UserCar> find = new Finder<String,UserCar>(UserCar.class);

	
	public static UserCar CreateNewUserCar(int userId, String carName, int comfortLevel) {
		UserCar userCar = new UserCar();
		
		userCar.userId = userId;
		userCar.carName = carName;
		userCar.comfortLavel = comfortLevel;
		
		Ebean.save(userCar);
		return userCar;
	}
	
	public static UserCar getUserCarById(int newUserCarid) {
		UserCar userCar = Ebean.find(UserCar.class).where().eq("user_car_id", newUserCarid).findUnique();
		return userCar;
	}
	
	public static List<UserCar> getUserCarByUserId(int newUserId) {
		List<UserCar> userCar = Ebean.find(UserCar.class).where().eq("user_id", newUserId).findList();
		return userCar;
	}
	
	public static UserCar getUserCarByUserIdCarName(int newUserId, String carName) {
		List<UserCar> userCarList = Ebean.find(UserCar.class).where().eq("user_id", newUserId).findList();
		
		for(UserCar usercar : userCarList) {
			if(usercar.carName.equals(carName)) {
				return usercar;
			}
				
		}
		return null;
	}
}
