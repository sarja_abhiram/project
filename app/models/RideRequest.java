package models;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import play.libs.Json;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Entity
public class RideRequest extends Model{

	@Id
	public int rideRequestId;

	@ManyToOne
	@JoinColumn(table="User", referencedColumnName="userId")
	public int userId;


	@ManyToOne
	@JoinColumn(table="Area", referencedColumnName="areaId")
	public int sourceAreaId;

	@ManyToOne
	@JoinColumn(table="Area", referencedColumnName="areaId")
	public int destinationAreaId;

	public Date createdTime;

	public Timestamp  travelTime ;

	public int seats;


	@ManyToOne
	@JoinColumn(table="Status", referencedColumnName="statusId")
	public int RequestStatus;

	public static Finder<String, RideRequest> find = new Finder<String,RideRequest>(RideRequest.class);

	public static void createRideRequest(int userId, int sourceAreaId, int destinationAreaId, Timestamp time, int seats) {
		RideRequest rideRequest = new RideRequest();
		rideRequest.userId = userId;
		rideRequest.sourceAreaId = sourceAreaId;
		rideRequest.destinationAreaId = destinationAreaId;
		rideRequest.seats = seats;
		rideRequest.createdTime = new Date();
		rideRequest.travelTime = time;
		Ebean.save(rideRequest);
	}

	public static RideRequest getRideRequestById(int userId) throws NullPointerException{
		RideRequest rideRequest = Ebean.find(RideRequest.class).where().eq("user_id", userId).findUnique();
		return rideRequest;
	}

	public static RideRequest getRideRequest(int rideRequestId) {
		RideRequest rideRequest = Ebean.find(RideRequest.class).where().eq("ride_request_id", rideRequestId).findUnique();
		return rideRequest;
	}
	public static ObjectNode PlaceRideRequest(Session session, String fromArea, String toArea, Timestamp time, int seats) {

		ObjectNode result = Json.newObject();

		try{	
			int sourceAreaId = Area.getAreaId(fromArea);
			int destinationAreaId = Area.getAreaId(toArea);

			RideRequest.createRideRequest(session.userId, sourceAreaId, destinationAreaId, time,  seats);

			result.put("response", "success");
			result.put("userId", session.userId);
			result.put("userName", session.userName);
			return result;
		}
		catch(Exception e) {
			result.put("response", "unsuccess");
			return result;
		}

	}

	public static JsonNode matchRequests(Session session) {
		
		int requesterId = session.userId;

		RideRequest newRideRequest = RideRequest.getRideRequestById(requesterId);
		int requestId = newRideRequest.rideRequestId;
		int destAreaId = newRideRequest.destinationAreaId;
		
		try {	
			String query = "select u.user_id, u.user_name, u.email, u.contact, uc.car_name, uc.comfort_lavel, r.ride_id, r.travel_time, r.cost_per_head " +
					"from user u, user_car uc, ride r " +
					"where u.user_id = uc.user_id and uc.user_car_id = r.user_car_id and r.ride_id IN " +
					"(select ri.ride_id from ride ri, ride_request rq  " +
					"where ride_request_id = " +  requestId +" and ri.source_area_id = rq.source_area_id and " +
					"ri.destination_area_id = rq.destination_area_id and rq.seats <= ri.seats and UNIX_TIMESTAMP(ri.travel_time) - UNIX_TIMESTAMP(rq.travel_time) <= 900 and " +
					"UNIX_TIMESTAMP(ri.travel_time) - UNIX_TIMESTAMP(( SELECT travel_time from project.ride_request where ride_request_id= "+ requestId +" )) >= 0)" +
					" UNION " +
					"select u.user_id, u.user_name, u.email, u.contact, uc.car_name, uc.comfort_lavel, r.ride_id, r.travel_time, r.cost_per_head " +
					"from user u, user_car uc, ride r where u.user_id = uc.user_id and uc.user_car_id = r.user_car_id " +
					"and r.ride_id IN " +
					"(select temp.ride_id from ( SELECT enroute_area_id, ride_id from project.enroute_area where ride_id in " +
					"(SELECT ride_id FROM project.ride ri, project.ride_request rq where rq.seats <= ri.seats and ri.source_area_id = " +
					"( SELECT source_area_id from project.ride_request where ride_request_id= " + requestId + " ) " +
					"and UNIX_TIMESTAMP(ri.travel_time) - " +
					"UNIX_TIMESTAMP(( SELECT travel_time from project.ride_request where ride_request_id= "+ requestId +" )) <= 900 and UNIX_TIMESTAMP(ri.travel_time) - " +
					"UNIX_TIMESTAMP(( SELECT travel_time from project.ride_request where ride_request_id= "+ requestId +" )) >= 0 )) " +
					"as temp where temp.enroute_area_id =  " + destAreaId +" )";

			SqlQuery queryObj = Ebean.createSqlQuery(query);
			List<SqlRow> driversList = queryObj.findList();
			JsonNode driversJson = Json.toJson(driversList);
			

			for(SqlRow obj : driversList) {
				
				if(!(Requests.isRequestPresent(requestId, obj.getInteger("ride_id")))) {
					Requests requests = new Requests();
					requests.rideId = obj.getInteger("ride_id");
					requests.rideRequestId = requestId;
					requests.userStatus = "matched";
					requests.driverStatus = "matched";
					Ebean.save(requests);
				}
	
			}
						
			return driversJson;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ObjectNode updateRideRequest(Session session, String sourceArea, String destArea, Timestamp timeOfTravel,
			int seats) {

		ObjectNode result = Json.newObject();

		try{	
			int sourceAreaId = Area.getAreaId(sourceArea);
			int destinationAreaId = Area.getAreaId(destArea);

			SqlUpdate updateQuery = Ebean.createSqlUpdate("UPDATE ride_request set source_area_id= :param1, destination_area_id = :param2," +
					" travel_time = :param3, seats = :param4 " +
					"where user_id= :param5");;
			
			updateQuery.setParameter("param1", sourceAreaId);
			updateQuery.setParameter("param2", destinationAreaId);
			updateQuery.setParameter("param3", timeOfTravel);
			updateQuery.setParameter("param4", seats);
			updateQuery.setParameter("param5", session.userId);
			updateQuery.execute();
			
			result.put("response", "success");
			result.put("userId", session.userId);
			result.put("userName", session.userName);
			return result;
		}
		catch(Exception e) {
			result.put("response", "unsuccess");
			return result;
		}
	}

	public static ObjectNode loadRideRequestDetails(Session session) throws Exception{
	
		ObjectNode result = Json.newObject();
		
		RideRequest rideRequest = RideRequest.getRideRequestById(session.userId);
		
		String source = Area.getAreaName(rideRequest.sourceAreaId);
		String destination = Area.getAreaName(rideRequest.destinationAreaId);
		JsonNode jsonTravelTime = Json.toJson(rideRequest.travelTime);
		
		result.put("from", source);
		result.put("to", destination);
		result.put("time", jsonTravelTime);
		result.put("seats", rideRequest.seats);
		return result;
	}
	
}