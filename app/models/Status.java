package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

@Entity
public class Status extends Model{

	@Id
	public int statusId;
	
	public String description;
	
	public static Finder<String, Status> find = new Finder<String,Status>(Status.class);
}
