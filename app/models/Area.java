package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.libs.Json;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.JsonNode;

@Entity
public class Area extends Model {
	
	@Id
	public int areaId;
	
	public String areaName;
	public String city;

	public static Finder<String, Area> find = new Finder<String,Area>(Area.class);
	
	public static String getAreaName(int id) {
		Area area = Ebean.find(Area.class).where().eq("area_id",id).findUnique();
		
		return area.areaName;
	}
	
	public static int getAreaId(String areaName) {
		Area area = Ebean.find(Area.class).where().eq("area_name", areaName).findUnique();
		return area.areaId;
	}
	
	public static JsonNode autoPopulation(String enteredString) {
		
	try{
		String query = "select area_name from area where area_name like '" + enteredString +"%'";
		SqlQuery queryObj = Ebean.createSqlQuery(query);
		List<SqlRow> areasList = queryObj.findList();
		
		JsonNode areasJson = Json.toJson(areasList);
		return areasJson;
	}
	catch(Exception e) {
		e.printStackTrace();
		return null;
	}	
  }
}
