package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;


@Entity
public class Car extends Model{

	@Id
	public int carID;
	
	@NotNull
	public String carName;
	
	@NotNull
	public String carModel;
	
	@NotNull
	public int comfortLevel;
	
	public static Finder<String, Car> find = new Finder<String,Car>(Car.class);
	
	
	
	public static boolean isCarExists(String carName) {

		try {
			Car car = Ebean.find(Car.class).where().eq("car_name", carName).findUnique();
	
		}
		catch(NullPointerException e) {
			e.printStackTrace();
			return false;
		}
	return true;
	}
	
	public static Car getCarByName(String carName) {
		Car car = Ebean.find(Car.class).where().eq("car_name", carName).findUnique();
		return car;
	}

	public static Car createNewCar(String carName, String carModel, int comfortLevel) {
		Car car = new Car();
		car.carName = carName;
		car.carModel = carModel;
		car.comfortLevel = comfortLevel;
		Ebean.save(car);
		
		return car;
	}
}
