package models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.libs.Json;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


@Entity
public class Ride extends Model{

	@Id
	public int rideId;
	
	@ManyToOne
	@JoinColumn(table="User_Car", referencedColumnName="UserCarId")
	public int userCarId;
	
	public Date createdDate;
	
	public Timestamp travelTime;
	
	@ManyToOne
	@JoinColumn(table="Area", referencedColumnName="areaId")
	public int sourceAreaId;
	
	@ManyToOne
	@JoinColumn(table="Area", referencedColumnName="areaId")
	public int destinationAreaId;
	
	public int seats;
	public Double costPerHead;
	
	
	public static Finder<String, Ride> find = new Finder<String,Ride>(Ride.class);
	
	public static Ride CreateRide(int userCarId, int sourceAreaId, int destinationAreaId, Timestamp time, int seats, double costPerHead) {
		Ride ride = new Ride();
		ride.userCarId = userCarId;
		ride.sourceAreaId = sourceAreaId;
		ride.destinationAreaId = destinationAreaId;
		ride.seats = seats;
		ride.createdDate = new Date();
		ride.travelTime = time;
		ride.costPerHead = costPerHead;
		Ebean.save(ride);
		return ride;
	}

	public static Ride getRideById(int rideId) {
		Ride ride = Ebean.find(Ride.class).where().eq("ride_id", rideId).findUnique();
		return ride;
	}
	
	public static int getUserCarId(int rideId) {
		Ride  ride = Ebean.find(Ride.class).where().eq("ride_id", rideId).findUnique();
		return ride.userCarId;
	}
	
	public static Ride getRideByUserCarId(int userCarId) throws Exception{
		Ride ride = Ebean.find(Ride.class).where().eq("user_car_id", userCarId).findUnique();
		return ride;
	}

	public static Ride UpdateRide(int userCarId, int sourceAreaId, int destinationAreaId, Timestamp timeOfTravel, int seats,
			Double costPerHead){

		SqlUpdate updateQuery = Ebean.createSqlUpdate("UPDATE ride set source_area_id= :param1, destination_area_id = :param2," +
				" travel_time = :param3, seats = :param4, cost_per_head = :param5 " +
				"where user_car_id= :param6");
		
		updateQuery.setParameter("param1", sourceAreaId);
		updateQuery.setParameter("param2", destinationAreaId);
		updateQuery.setParameter("param3", timeOfTravel);
		updateQuery.setParameter("param4", seats);
		updateQuery.setParameter("param5", costPerHead);
		updateQuery.setParameter("param6", userCarId);
		updateQuery.execute();
		
		Ride ride = Ebean.find(Ride.class).where().eq("user_car_id", userCarId).findUnique();
		return ride;
	}

	public static ObjectNode updateExistingRideByUser(Session session, int rideId, int userCarId, String sourceArea, String destArea,
			Timestamp timeOfTravel, String[] viaAreas, int seatsAvailable, double costPerHead) {
		
		ObjectNode result = Json.newObject();
		int areaId = 0;
		
	try {
		
		int sourceAreaId = Area.getAreaId(sourceArea);
		int destinationAreaId = Area.getAreaId(destArea);

		Ride ride = Ride.UpdateRideByRideId(rideId, userCarId, sourceAreaId, destinationAreaId, timeOfTravel, seatsAvailable, costPerHead);
		
		EnrouteArea.deleteEnrouteAreas(ride.rideId);
		
		for(String areaname : viaAreas) {
			areaId= Area.getAreaId(areaname);
			EnrouteArea.createEnrouteAreas(areaId, ride.rideId);
		}

		result.put("sessionKey", session.sessionId);
		result.put("status", "success");

		return result;
		}
	catch(Exception e){
		
		result.put("status", "Unsuccess");
		e.printStackTrace();
		return result;
		
	}
}

	private static Ride UpdateRideByRideId(int rideId, int userCarId, int sourceAreaId, int destinationAreaId, 
			Timestamp timeOfTravel,int seatsAvailable, double costPerHead) {
		
		SqlUpdate updateQuery = Ebean.createSqlUpdate("UPDATE ride set source_area_id= :param1, destination_area_id = :param2," +
				" travel_time = :param3, seats = :param4, cost_per_head = :param5, user_car_id = :param7 " +
				"where ride_id= :param6");
		
		updateQuery.setParameter("param1", sourceAreaId);
		updateQuery.setParameter("param2", destinationAreaId);
		updateQuery.setParameter("param3", timeOfTravel);
		updateQuery.setParameter("param4", seatsAvailable);
		updateQuery.setParameter("param5", costPerHead);
		updateQuery.setParameter("param6", rideId);
		updateQuery.setParameter("param7", userCarId);
		updateQuery.execute();
		
		Ride ride = Ebean.find(Ride.class).where().eq("user_car_id", userCarId).findUnique();
		return ride;
	}

	public static ObjectNode loadRideDetails(Session session) throws Exception{
		
		ObjectNode result = Json.newObject();
		
		List<UserCar> userCarList = UserCar.getUserCarByUserId(session.userId);
		Ride ride = new Ride();
		List<String> areasList = new ArrayList<String>();
		
		for(UserCar usercar : userCarList) {
			ride = Ebean.find(Ride.class).where().eq("user_car_id", usercar.userCarId).findUnique();
			
			if(ride != null) {
				List<EnrouteArea> enrouteAreaList = EnrouteArea.getEnrouteAreasByRideId(ride.rideId);
				
				for(EnrouteArea enroutearea : enrouteAreaList){
					String areaName = Area.getAreaName(enroutearea.enrouteAreaId);
					areasList.add(areaName);
				}
				String source = Area.getAreaName(ride.sourceAreaId);
				String destination = Area.getAreaName(ride.destinationAreaId);
				
				JsonNode jsonAreasList = Json.toJson(areasList);
				JsonNode jsonTravelTime = Json.toJson(ride.travelTime);
				
				result.put("from", source);
				result.put("to", destination);
				result.put("via", jsonAreasList);
				result.put("travelTime", jsonTravelTime);
				return result;						
			}
		}
		return null;
	}
}
