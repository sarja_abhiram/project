# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table area (
  area_id                       integer auto_increment not null,
  area_name                     varchar(255),
  city                          varchar(255),
  constraint pk_area primary key (area_id)
);

create table car (
  car_id                        integer auto_increment not null,
  car_name                      varchar(255) not null,
  car_model                     varchar(255) not null,
  comfort_level                 integer not null,
  constraint pk_car primary key (car_id)
);

create table enroute_area (
  enroute_id                    integer auto_increment not null,
  enroute_area_id               integer,
  ride_id                       integer,
  constraint pk_enroute_area primary key (enroute_id)
);

create table requests (
  request_id                    integer auto_increment not null,
  ride_id                       integer,
  ride_request_id               integer,
  user_status                   varchar(255),
  driver_status                 varchar(255),
  constraint pk_requests primary key (request_id)
);

create table ride (
  ride_id                       integer auto_increment not null,
  user_car_id                   integer,
  created_date                  datetime(6),
  travel_time                   datetime(6),
  source_area_id                integer,
  destination_area_id           integer,
  seats                         integer,
  cost_per_head                 double,
  constraint pk_ride primary key (ride_id)
);

create table ride_request (
  ride_request_id               integer auto_increment not null,
  user_id                       integer,
  source_area_id                integer,
  destination_area_id           integer,
  created_time                  datetime(6),
  travel_time                   datetime(6),
  seats                         integer,
  request_status                integer,
  constraint pk_ride_request primary key (ride_request_id)
);

create table session (
  user_id                       integer auto_increment not null,
  user_name                     varchar(255),
  session_id                    varchar(255),
  constraint pk_session primary key (user_id)
);

create table status (
  status_id                     integer auto_increment not null,
  description                   varchar(255),
  constraint pk_status primary key (status_id)
);

create table user (
  user_id                       integer auto_increment not null,
  user_name                     varchar(255),
  password                      varchar(255) not null,
  email                         varchar(255) not null,
  contact                       varchar(255) not null,
  dlnumber                      varchar(255),
  constraint pk_user primary key (user_id)
);

create table user_car (
  user_car_id                   integer auto_increment not null,
  user_id                       integer,
  car_name                      varchar(255),
  comfort_lavel                 integer,
  constraint pk_user_car primary key (user_car_id)
);


# --- !Downs

drop table if exists area;

drop table if exists car;

drop table if exists enroute_area;

drop table if exists requests;

drop table if exists ride;

drop table if exists ride_request;

drop table if exists session;

drop table if exists status;

drop table if exists user;

drop table if exists user_car;

