
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/fresher/PlayJava/activator-dist-1.3.10/bin/Car-Pooling/conf/routes
// @DATE:Sat Sep 10 17:52:35 IST 2016

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:5
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:57
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:57
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:45
  class ReverseLoadPassengerDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:45
    def loadPassengerDetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoadPassengerDetailsController.loadPassengerDetails",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "onPassengerLoad"})
        }
      """
    )
  
  }

  // @LINE:49
  class ReverseTakeRideController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:49
    def onTakeRide: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.TakeRideController.onTakeRide",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "onTakeRide"})
        }
      """
    )
  
    // @LINE:51
    def onTakeRideAgain: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.TakeRideController.onTakeRideAgain",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "onTakeRideAgain"})
        }
      """
    )
  
  }

  // @LINE:19
  class ReversePassengerRequestController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:19
    def OnPassengerRequest: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PassengerRequestController.OnPassengerRequest",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "passengerRequest"})
        }
      """
    )
  
  }

  // @LINE:27
  class ReversePassengerRefreshController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:27
    def DriverToPassenger: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PassengerRefreshController.DriverToPassenger",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "passengerDriverAccept"})
        }
      """
    )
  
  }

  // @LINE:13
  class ReverseMatchRequestsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def MatchRequest: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MatchRequestsController.MatchRequest",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "matchRequest"})
        }
      """
    )
  
  }

  // @LINE:31
  class ReverseKnowMyDriverController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:31
    def knowMyDriver: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.KnowMyDriverController.knowMyDriver",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "knowMyDriver"})
        }
      """
    )
  
  }

  // @LINE:15
  class ReverseLogOutController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:17
    def logOutAgain: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LogOutController.logOutAgain",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "logOutAgain"})
        }
      """
    )
  
    // @LINE:15
    def logOut: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LogOutController.logOut",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logOut"})
        }
      """
    )
  
  }

  // @LINE:43
  class ReverseLoadDriverDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:43
    def onLoadingDriver: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoadDriverDetailsController.onLoadingDriver",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "onDriverPageLoad"})
        }
      """
    )
  
  }

  // @LINE:9
  class ReverseDriverDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def driverDetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DriverDetailsController.driverDetails",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverDetails"})
        }
      """
    )
  
  }

  // @LINE:5
  class ReverseSignupController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:5
    def signUpUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SignupController.signUpUser",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "signUpUser"})
        }
      """
    )
  
    // @LINE:54
    def preflight: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SignupController.preflight",
      """
        function(path0) {
          return _wA({method:"OPTIONS", url:"""" + _prefix + { _defaultPrefix } + """" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", path0)})
        }
      """
    )
  
  }

  // @LINE:7
  class ReverseLoginController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def loginUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.loginUser",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "loginUser"})
        }
      """
    )
  
  }

  // @LINE:25
  class ReverseDriverAcceptController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:25
    def OnDriverAccept: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DriverAcceptController.OnDriverAccept",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverAccepts"})
        }
      """
    )
  
  }

  // @LINE:39
  class ReverseCarDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:39
    def addCarDetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CarDetailsController.addCarDetails",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addCar"})
        }
      """
    )
  
    // @LINE:41
    def getCarDetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CarDetailsController.getCarDetails",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getCarDetails"})
        }
      """
    )
  
  }

  // @LINE:47
  class ReverseGiveRideController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:47
    def onGiveRide: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GiveRideController.onGiveRide",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "onGiveRide"})
        }
      """
    )
  
  }

  // @LINE:29
  class ReverseAutoPopulationController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def autoPopulation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AutoPopulationController.autoPopulation",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "autoPopulation"})
        }
      """
    )
  
  }

  // @LINE:37
  class ReverseDriverFinalViewController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:37
    def whoAccepted: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DriverFinalViewController.whoAccepted",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "whoAcceptedMyRide"})
        }
      """
    )
  
  }

  // @LINE:21
  class ReverseDriverRefreshController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:21
    def onDriverRefresh: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DriverRefreshController.onDriverRefresh",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getPassengers"})
        }
      """
    )
  
    // @LINE:23
    def onDriverCancel: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.DriverRefreshController.onDriverCancel",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getAllPassengers"})
        }
      """
    )
  
  }

  // @LINE:11
  class ReversePreferenceController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def requestRide: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PreferenceController.requestRide",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "requestRide"})
        }
      """
    )
  
  }

  // @LINE:33
  class ReversePassengerReactsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:33
    def cancelRequest: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PassengerReactsController.cancelRequest",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "cancelRequest"})
        }
      """
    )
  
    // @LINE:35
    def acceptRequest: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PassengerReactsController.acceptRequest",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "acceptRequest"})
        }
      """
    )
  
  }


}
