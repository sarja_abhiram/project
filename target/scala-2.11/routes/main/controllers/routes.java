
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/fresher/PlayJava/activator-dist-1.3.10/bin/Car-Pooling/conf/routes
// @DATE:Sat Sep 10 17:52:35 IST 2016

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLoadPassengerDetailsController LoadPassengerDetailsController = new controllers.ReverseLoadPassengerDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseTakeRideController TakeRideController = new controllers.ReverseTakeRideController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePassengerRequestController PassengerRequestController = new controllers.ReversePassengerRequestController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePassengerRefreshController PassengerRefreshController = new controllers.ReversePassengerRefreshController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseMatchRequestsController MatchRequestsController = new controllers.ReverseMatchRequestsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseKnowMyDriverController KnowMyDriverController = new controllers.ReverseKnowMyDriverController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLogOutController LogOutController = new controllers.ReverseLogOutController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLoadDriverDetailsController LoadDriverDetailsController = new controllers.ReverseLoadDriverDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseDriverDetailsController DriverDetailsController = new controllers.ReverseDriverDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSignupController SignupController = new controllers.ReverseSignupController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLoginController LoginController = new controllers.ReverseLoginController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseDriverAcceptController DriverAcceptController = new controllers.ReverseDriverAcceptController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCarDetailsController CarDetailsController = new controllers.ReverseCarDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGiveRideController GiveRideController = new controllers.ReverseGiveRideController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAutoPopulationController AutoPopulationController = new controllers.ReverseAutoPopulationController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseDriverFinalViewController DriverFinalViewController = new controllers.ReverseDriverFinalViewController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseDriverRefreshController DriverRefreshController = new controllers.ReverseDriverRefreshController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePreferenceController PreferenceController = new controllers.ReversePreferenceController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePassengerReactsController PassengerReactsController = new controllers.ReversePassengerReactsController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLoadPassengerDetailsController LoadPassengerDetailsController = new controllers.javascript.ReverseLoadPassengerDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseTakeRideController TakeRideController = new controllers.javascript.ReverseTakeRideController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePassengerRequestController PassengerRequestController = new controllers.javascript.ReversePassengerRequestController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePassengerRefreshController PassengerRefreshController = new controllers.javascript.ReversePassengerRefreshController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseMatchRequestsController MatchRequestsController = new controllers.javascript.ReverseMatchRequestsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseKnowMyDriverController KnowMyDriverController = new controllers.javascript.ReverseKnowMyDriverController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLogOutController LogOutController = new controllers.javascript.ReverseLogOutController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLoadDriverDetailsController LoadDriverDetailsController = new controllers.javascript.ReverseLoadDriverDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseDriverDetailsController DriverDetailsController = new controllers.javascript.ReverseDriverDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSignupController SignupController = new controllers.javascript.ReverseSignupController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLoginController LoginController = new controllers.javascript.ReverseLoginController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseDriverAcceptController DriverAcceptController = new controllers.javascript.ReverseDriverAcceptController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCarDetailsController CarDetailsController = new controllers.javascript.ReverseCarDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGiveRideController GiveRideController = new controllers.javascript.ReverseGiveRideController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAutoPopulationController AutoPopulationController = new controllers.javascript.ReverseAutoPopulationController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseDriverFinalViewController DriverFinalViewController = new controllers.javascript.ReverseDriverFinalViewController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseDriverRefreshController DriverRefreshController = new controllers.javascript.ReverseDriverRefreshController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePreferenceController PreferenceController = new controllers.javascript.ReversePreferenceController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePassengerReactsController PassengerReactsController = new controllers.javascript.ReversePassengerReactsController(RoutesPrefix.byNamePrefix());
  }

}
