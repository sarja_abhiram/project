
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/fresher/PlayJava/activator-dist-1.3.10/bin/Car-Pooling/conf/routes
// @DATE:Sat Sep 10 17:52:35 IST 2016


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
