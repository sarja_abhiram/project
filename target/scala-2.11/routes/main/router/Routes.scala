
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/fresher/PlayJava/activator-dist-1.3.10/bin/Car-Pooling/conf/routes
// @DATE:Sat Sep 10 17:52:35 IST 2016

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:5
  SignupController_6: controllers.SignupController,
  // @LINE:7
  LoginController_17: controllers.LoginController,
  // @LINE:9
  DriverDetailsController_9: controllers.DriverDetailsController,
  // @LINE:11
  PreferenceController_7: controllers.PreferenceController,
  // @LINE:13
  MatchRequestsController_14: controllers.MatchRequestsController,
  // @LINE:15
  LogOutController_11: controllers.LogOutController,
  // @LINE:19
  PassengerRequestController_2: controllers.PassengerRequestController,
  // @LINE:21
  DriverRefreshController_19: controllers.DriverRefreshController,
  // @LINE:25
  DriverAcceptController_1: controllers.DriverAcceptController,
  // @LINE:27
  PassengerRefreshController_13: controllers.PassengerRefreshController,
  // @LINE:29
  AutoPopulationController_8: controllers.AutoPopulationController,
  // @LINE:31
  KnowMyDriverController_10: controllers.KnowMyDriverController,
  // @LINE:33
  PassengerReactsController_12: controllers.PassengerReactsController,
  // @LINE:37
  DriverFinalViewController_0: controllers.DriverFinalViewController,
  // @LINE:39
  CarDetailsController_18: controllers.CarDetailsController,
  // @LINE:43
  LoadDriverDetailsController_15: controllers.LoadDriverDetailsController,
  // @LINE:45
  LoadPassengerDetailsController_3: controllers.LoadPassengerDetailsController,
  // @LINE:47
  GiveRideController_5: controllers.GiveRideController,
  // @LINE:49
  TakeRideController_4: controllers.TakeRideController,
  // @LINE:57
  Assets_16: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:5
    SignupController_6: controllers.SignupController,
    // @LINE:7
    LoginController_17: controllers.LoginController,
    // @LINE:9
    DriverDetailsController_9: controllers.DriverDetailsController,
    // @LINE:11
    PreferenceController_7: controllers.PreferenceController,
    // @LINE:13
    MatchRequestsController_14: controllers.MatchRequestsController,
    // @LINE:15
    LogOutController_11: controllers.LogOutController,
    // @LINE:19
    PassengerRequestController_2: controllers.PassengerRequestController,
    // @LINE:21
    DriverRefreshController_19: controllers.DriverRefreshController,
    // @LINE:25
    DriverAcceptController_1: controllers.DriverAcceptController,
    // @LINE:27
    PassengerRefreshController_13: controllers.PassengerRefreshController,
    // @LINE:29
    AutoPopulationController_8: controllers.AutoPopulationController,
    // @LINE:31
    KnowMyDriverController_10: controllers.KnowMyDriverController,
    // @LINE:33
    PassengerReactsController_12: controllers.PassengerReactsController,
    // @LINE:37
    DriverFinalViewController_0: controllers.DriverFinalViewController,
    // @LINE:39
    CarDetailsController_18: controllers.CarDetailsController,
    // @LINE:43
    LoadDriverDetailsController_15: controllers.LoadDriverDetailsController,
    // @LINE:45
    LoadPassengerDetailsController_3: controllers.LoadPassengerDetailsController,
    // @LINE:47
    GiveRideController_5: controllers.GiveRideController,
    // @LINE:49
    TakeRideController_4: controllers.TakeRideController,
    // @LINE:57
    Assets_16: controllers.Assets
  ) = this(errorHandler, SignupController_6, LoginController_17, DriverDetailsController_9, PreferenceController_7, MatchRequestsController_14, LogOutController_11, PassengerRequestController_2, DriverRefreshController_19, DriverAcceptController_1, PassengerRefreshController_13, AutoPopulationController_8, KnowMyDriverController_10, PassengerReactsController_12, DriverFinalViewController_0, CarDetailsController_18, LoadDriverDetailsController_15, LoadPassengerDetailsController_3, GiveRideController_5, TakeRideController_4, Assets_16, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, SignupController_6, LoginController_17, DriverDetailsController_9, PreferenceController_7, MatchRequestsController_14, LogOutController_11, PassengerRequestController_2, DriverRefreshController_19, DriverAcceptController_1, PassengerRefreshController_13, AutoPopulationController_8, KnowMyDriverController_10, PassengerReactsController_12, DriverFinalViewController_0, CarDetailsController_18, LoadDriverDetailsController_15, LoadPassengerDetailsController_3, GiveRideController_5, TakeRideController_4, Assets_16, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signUpUser""", """controllers.SignupController.signUpUser"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """loginUser""", """controllers.LoginController.loginUser"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """driverDetails""", """controllers.DriverDetailsController.driverDetails"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """requestRide""", """controllers.PreferenceController.requestRide"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """matchRequest""", """controllers.MatchRequestsController.MatchRequest"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logOut""", """controllers.LogOutController.logOut"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logOutAgain""", """controllers.LogOutController.logOutAgain"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """passengerRequest""", """controllers.PassengerRequestController.OnPassengerRequest"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getPassengers""", """controllers.DriverRefreshController.onDriverRefresh"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getAllPassengers""", """controllers.DriverRefreshController.onDriverCancel"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """driverAccepts""", """controllers.DriverAcceptController.OnDriverAccept"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """passengerDriverAccept""", """controllers.PassengerRefreshController.DriverToPassenger"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """autoPopulation""", """controllers.AutoPopulationController.autoPopulation"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """knowMyDriver""", """controllers.KnowMyDriverController.knowMyDriver"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cancelRequest""", """controllers.PassengerReactsController.cancelRequest"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """acceptRequest""", """controllers.PassengerReactsController.acceptRequest"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """whoAcceptedMyRide""", """controllers.DriverFinalViewController.whoAccepted"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """addCar""", """controllers.CarDetailsController.addCarDetails"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCarDetails""", """controllers.CarDetailsController.getCarDetails"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """onDriverPageLoad""", """controllers.LoadDriverDetailsController.onLoadingDriver"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """onPassengerLoad""", """controllers.LoadPassengerDetailsController.loadPassengerDetails"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """onGiveRide""", """controllers.GiveRideController.onGiveRide"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """onTakeRide""", """controllers.TakeRideController.onTakeRide"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """onTakeRideAgain""", """controllers.TakeRideController.onTakeRideAgain"""),
    ("""OPTIONS""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """path<.+>""", """controllers.SignupController.preflight(path:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:5
  private[this] lazy val controllers_SignupController_signUpUser0_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signUpUser")))
  )
  private[this] lazy val controllers_SignupController_signUpUser0_invoker = createInvoker(
    SignupController_6.signUpUser,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SignupController",
      "signUpUser",
      Nil,
      "POST",
      """""",
      this.prefix + """signUpUser"""
    )
  )

  // @LINE:7
  private[this] lazy val controllers_LoginController_loginUser1_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("loginUser")))
  )
  private[this] lazy val controllers_LoginController_loginUser1_invoker = createInvoker(
    LoginController_17.loginUser,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "loginUser",
      Nil,
      "POST",
      """""",
      this.prefix + """loginUser"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_DriverDetailsController_driverDetails2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("driverDetails")))
  )
  private[this] lazy val controllers_DriverDetailsController_driverDetails2_invoker = createInvoker(
    DriverDetailsController_9.driverDetails,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DriverDetailsController",
      "driverDetails",
      Nil,
      "POST",
      """""",
      this.prefix + """driverDetails"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_PreferenceController_requestRide3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("requestRide")))
  )
  private[this] lazy val controllers_PreferenceController_requestRide3_invoker = createInvoker(
    PreferenceController_7.requestRide,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PreferenceController",
      "requestRide",
      Nil,
      "POST",
      """""",
      this.prefix + """requestRide"""
    )
  )

  // @LINE:13
  private[this] lazy val controllers_MatchRequestsController_MatchRequest4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("matchRequest")))
  )
  private[this] lazy val controllers_MatchRequestsController_MatchRequest4_invoker = createInvoker(
    MatchRequestsController_14.MatchRequest,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MatchRequestsController",
      "MatchRequest",
      Nil,
      "GET",
      """""",
      this.prefix + """matchRequest"""
    )
  )

  // @LINE:15
  private[this] lazy val controllers_LogOutController_logOut5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logOut")))
  )
  private[this] lazy val controllers_LogOutController_logOut5_invoker = createInvoker(
    LogOutController_11.logOut,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LogOutController",
      "logOut",
      Nil,
      "GET",
      """""",
      this.prefix + """logOut"""
    )
  )

  // @LINE:17
  private[this] lazy val controllers_LogOutController_logOutAgain6_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logOutAgain")))
  )
  private[this] lazy val controllers_LogOutController_logOutAgain6_invoker = createInvoker(
    LogOutController_11.logOutAgain,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LogOutController",
      "logOutAgain",
      Nil,
      "POST",
      """""",
      this.prefix + """logOutAgain"""
    )
  )

  // @LINE:19
  private[this] lazy val controllers_PassengerRequestController_OnPassengerRequest7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("passengerRequest")))
  )
  private[this] lazy val controllers_PassengerRequestController_OnPassengerRequest7_invoker = createInvoker(
    PassengerRequestController_2.OnPassengerRequest,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PassengerRequestController",
      "OnPassengerRequest",
      Nil,
      "POST",
      """""",
      this.prefix + """passengerRequest"""
    )
  )

  // @LINE:21
  private[this] lazy val controllers_DriverRefreshController_onDriverRefresh8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getPassengers")))
  )
  private[this] lazy val controllers_DriverRefreshController_onDriverRefresh8_invoker = createInvoker(
    DriverRefreshController_19.onDriverRefresh,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DriverRefreshController",
      "onDriverRefresh",
      Nil,
      "GET",
      """""",
      this.prefix + """getPassengers"""
    )
  )

  // @LINE:23
  private[this] lazy val controllers_DriverRefreshController_onDriverCancel9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getAllPassengers")))
  )
  private[this] lazy val controllers_DriverRefreshController_onDriverCancel9_invoker = createInvoker(
    DriverRefreshController_19.onDriverCancel,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DriverRefreshController",
      "onDriverCancel",
      Nil,
      "GET",
      """""",
      this.prefix + """getAllPassengers"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_DriverAcceptController_OnDriverAccept10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("driverAccepts")))
  )
  private[this] lazy val controllers_DriverAcceptController_OnDriverAccept10_invoker = createInvoker(
    DriverAcceptController_1.OnDriverAccept,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DriverAcceptController",
      "OnDriverAccept",
      Nil,
      "POST",
      """""",
      this.prefix + """driverAccepts"""
    )
  )

  // @LINE:27
  private[this] lazy val controllers_PassengerRefreshController_DriverToPassenger11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("passengerDriverAccept")))
  )
  private[this] lazy val controllers_PassengerRefreshController_DriverToPassenger11_invoker = createInvoker(
    PassengerRefreshController_13.DriverToPassenger,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PassengerRefreshController",
      "DriverToPassenger",
      Nil,
      "GET",
      """""",
      this.prefix + """passengerDriverAccept"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_AutoPopulationController_autoPopulation12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("autoPopulation")))
  )
  private[this] lazy val controllers_AutoPopulationController_autoPopulation12_invoker = createInvoker(
    AutoPopulationController_8.autoPopulation,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AutoPopulationController",
      "autoPopulation",
      Nil,
      "POST",
      """""",
      this.prefix + """autoPopulation"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_KnowMyDriverController_knowMyDriver13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("knowMyDriver")))
  )
  private[this] lazy val controllers_KnowMyDriverController_knowMyDriver13_invoker = createInvoker(
    KnowMyDriverController_10.knowMyDriver,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.KnowMyDriverController",
      "knowMyDriver",
      Nil,
      "GET",
      """""",
      this.prefix + """knowMyDriver"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_PassengerReactsController_cancelRequest14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cancelRequest")))
  )
  private[this] lazy val controllers_PassengerReactsController_cancelRequest14_invoker = createInvoker(
    PassengerReactsController_12.cancelRequest,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PassengerReactsController",
      "cancelRequest",
      Nil,
      "POST",
      """""",
      this.prefix + """cancelRequest"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_PassengerReactsController_acceptRequest15_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("acceptRequest")))
  )
  private[this] lazy val controllers_PassengerReactsController_acceptRequest15_invoker = createInvoker(
    PassengerReactsController_12.acceptRequest,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PassengerReactsController",
      "acceptRequest",
      Nil,
      "POST",
      """""",
      this.prefix + """acceptRequest"""
    )
  )

  // @LINE:37
  private[this] lazy val controllers_DriverFinalViewController_whoAccepted16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("whoAcceptedMyRide")))
  )
  private[this] lazy val controllers_DriverFinalViewController_whoAccepted16_invoker = createInvoker(
    DriverFinalViewController_0.whoAccepted,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.DriverFinalViewController",
      "whoAccepted",
      Nil,
      "GET",
      """""",
      this.prefix + """whoAcceptedMyRide"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_CarDetailsController_addCarDetails17_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("addCar")))
  )
  private[this] lazy val controllers_CarDetailsController_addCarDetails17_invoker = createInvoker(
    CarDetailsController_18.addCarDetails,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CarDetailsController",
      "addCarDetails",
      Nil,
      "POST",
      """""",
      this.prefix + """addCar"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_CarDetailsController_getCarDetails18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCarDetails")))
  )
  private[this] lazy val controllers_CarDetailsController_getCarDetails18_invoker = createInvoker(
    CarDetailsController_18.getCarDetails,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CarDetailsController",
      "getCarDetails",
      Nil,
      "GET",
      """""",
      this.prefix + """getCarDetails"""
    )
  )

  // @LINE:43
  private[this] lazy val controllers_LoadDriverDetailsController_onLoadingDriver19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("onDriverPageLoad")))
  )
  private[this] lazy val controllers_LoadDriverDetailsController_onLoadingDriver19_invoker = createInvoker(
    LoadDriverDetailsController_15.onLoadingDriver,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoadDriverDetailsController",
      "onLoadingDriver",
      Nil,
      "GET",
      """""",
      this.prefix + """onDriverPageLoad"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_LoadPassengerDetailsController_loadPassengerDetails20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("onPassengerLoad")))
  )
  private[this] lazy val controllers_LoadPassengerDetailsController_loadPassengerDetails20_invoker = createInvoker(
    LoadPassengerDetailsController_3.loadPassengerDetails,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoadPassengerDetailsController",
      "loadPassengerDetails",
      Nil,
      "GET",
      """""",
      this.prefix + """onPassengerLoad"""
    )
  )

  // @LINE:47
  private[this] lazy val controllers_GiveRideController_onGiveRide21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("onGiveRide")))
  )
  private[this] lazy val controllers_GiveRideController_onGiveRide21_invoker = createInvoker(
    GiveRideController_5.onGiveRide,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GiveRideController",
      "onGiveRide",
      Nil,
      "GET",
      """""",
      this.prefix + """onGiveRide"""
    )
  )

  // @LINE:49
  private[this] lazy val controllers_TakeRideController_onTakeRide22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("onTakeRide")))
  )
  private[this] lazy val controllers_TakeRideController_onTakeRide22_invoker = createInvoker(
    TakeRideController_4.onTakeRide,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TakeRideController",
      "onTakeRide",
      Nil,
      "GET",
      """""",
      this.prefix + """onTakeRide"""
    )
  )

  // @LINE:51
  private[this] lazy val controllers_TakeRideController_onTakeRideAgain23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("onTakeRideAgain")))
  )
  private[this] lazy val controllers_TakeRideController_onTakeRideAgain23_invoker = createInvoker(
    TakeRideController_4.onTakeRideAgain,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.TakeRideController",
      "onTakeRideAgain",
      Nil,
      "GET",
      """""",
      this.prefix + """onTakeRideAgain"""
    )
  )

  // @LINE:54
  private[this] lazy val controllers_SignupController_preflight24_route = Route("OPTIONS",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("path", """.+""",false)))
  )
  private[this] lazy val controllers_SignupController_preflight24_invoker = createInvoker(
    SignupController_6.preflight(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SignupController",
      "preflight",
      Seq(classOf[String]),
      "OPTIONS",
      """Pre flight path""",
      this.prefix + """""" + "$" + """path<.+>"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_Assets_versioned25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned25_invoker = createInvoker(
    Assets_16.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/""" + "$" + """file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:5
    case controllers_SignupController_signUpUser0_route(params) =>
      call { 
        controllers_SignupController_signUpUser0_invoker.call(SignupController_6.signUpUser)
      }
  
    // @LINE:7
    case controllers_LoginController_loginUser1_route(params) =>
      call { 
        controllers_LoginController_loginUser1_invoker.call(LoginController_17.loginUser)
      }
  
    // @LINE:9
    case controllers_DriverDetailsController_driverDetails2_route(params) =>
      call { 
        controllers_DriverDetailsController_driverDetails2_invoker.call(DriverDetailsController_9.driverDetails)
      }
  
    // @LINE:11
    case controllers_PreferenceController_requestRide3_route(params) =>
      call { 
        controllers_PreferenceController_requestRide3_invoker.call(PreferenceController_7.requestRide)
      }
  
    // @LINE:13
    case controllers_MatchRequestsController_MatchRequest4_route(params) =>
      call { 
        controllers_MatchRequestsController_MatchRequest4_invoker.call(MatchRequestsController_14.MatchRequest)
      }
  
    // @LINE:15
    case controllers_LogOutController_logOut5_route(params) =>
      call { 
        controllers_LogOutController_logOut5_invoker.call(LogOutController_11.logOut)
      }
  
    // @LINE:17
    case controllers_LogOutController_logOutAgain6_route(params) =>
      call { 
        controllers_LogOutController_logOutAgain6_invoker.call(LogOutController_11.logOutAgain)
      }
  
    // @LINE:19
    case controllers_PassengerRequestController_OnPassengerRequest7_route(params) =>
      call { 
        controllers_PassengerRequestController_OnPassengerRequest7_invoker.call(PassengerRequestController_2.OnPassengerRequest)
      }
  
    // @LINE:21
    case controllers_DriverRefreshController_onDriverRefresh8_route(params) =>
      call { 
        controllers_DriverRefreshController_onDriverRefresh8_invoker.call(DriverRefreshController_19.onDriverRefresh)
      }
  
    // @LINE:23
    case controllers_DriverRefreshController_onDriverCancel9_route(params) =>
      call { 
        controllers_DriverRefreshController_onDriverCancel9_invoker.call(DriverRefreshController_19.onDriverCancel)
      }
  
    // @LINE:25
    case controllers_DriverAcceptController_OnDriverAccept10_route(params) =>
      call { 
        controllers_DriverAcceptController_OnDriverAccept10_invoker.call(DriverAcceptController_1.OnDriverAccept)
      }
  
    // @LINE:27
    case controllers_PassengerRefreshController_DriverToPassenger11_route(params) =>
      call { 
        controllers_PassengerRefreshController_DriverToPassenger11_invoker.call(PassengerRefreshController_13.DriverToPassenger)
      }
  
    // @LINE:29
    case controllers_AutoPopulationController_autoPopulation12_route(params) =>
      call { 
        controllers_AutoPopulationController_autoPopulation12_invoker.call(AutoPopulationController_8.autoPopulation)
      }
  
    // @LINE:31
    case controllers_KnowMyDriverController_knowMyDriver13_route(params) =>
      call { 
        controllers_KnowMyDriverController_knowMyDriver13_invoker.call(KnowMyDriverController_10.knowMyDriver)
      }
  
    // @LINE:33
    case controllers_PassengerReactsController_cancelRequest14_route(params) =>
      call { 
        controllers_PassengerReactsController_cancelRequest14_invoker.call(PassengerReactsController_12.cancelRequest)
      }
  
    // @LINE:35
    case controllers_PassengerReactsController_acceptRequest15_route(params) =>
      call { 
        controllers_PassengerReactsController_acceptRequest15_invoker.call(PassengerReactsController_12.acceptRequest)
      }
  
    // @LINE:37
    case controllers_DriverFinalViewController_whoAccepted16_route(params) =>
      call { 
        controllers_DriverFinalViewController_whoAccepted16_invoker.call(DriverFinalViewController_0.whoAccepted)
      }
  
    // @LINE:39
    case controllers_CarDetailsController_addCarDetails17_route(params) =>
      call { 
        controllers_CarDetailsController_addCarDetails17_invoker.call(CarDetailsController_18.addCarDetails)
      }
  
    // @LINE:41
    case controllers_CarDetailsController_getCarDetails18_route(params) =>
      call { 
        controllers_CarDetailsController_getCarDetails18_invoker.call(CarDetailsController_18.getCarDetails)
      }
  
    // @LINE:43
    case controllers_LoadDriverDetailsController_onLoadingDriver19_route(params) =>
      call { 
        controllers_LoadDriverDetailsController_onLoadingDriver19_invoker.call(LoadDriverDetailsController_15.onLoadingDriver)
      }
  
    // @LINE:45
    case controllers_LoadPassengerDetailsController_loadPassengerDetails20_route(params) =>
      call { 
        controllers_LoadPassengerDetailsController_loadPassengerDetails20_invoker.call(LoadPassengerDetailsController_3.loadPassengerDetails)
      }
  
    // @LINE:47
    case controllers_GiveRideController_onGiveRide21_route(params) =>
      call { 
        controllers_GiveRideController_onGiveRide21_invoker.call(GiveRideController_5.onGiveRide)
      }
  
    // @LINE:49
    case controllers_TakeRideController_onTakeRide22_route(params) =>
      call { 
        controllers_TakeRideController_onTakeRide22_invoker.call(TakeRideController_4.onTakeRide)
      }
  
    // @LINE:51
    case controllers_TakeRideController_onTakeRideAgain23_route(params) =>
      call { 
        controllers_TakeRideController_onTakeRideAgain23_invoker.call(TakeRideController_4.onTakeRideAgain)
      }
  
    // @LINE:54
    case controllers_SignupController_preflight24_route(params) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_SignupController_preflight24_invoker.call(SignupController_6.preflight(path))
      }
  
    // @LINE:57
    case controllers_Assets_versioned25_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned25_invoker.call(Assets_16.versioned(path, file))
      }
  }
}
